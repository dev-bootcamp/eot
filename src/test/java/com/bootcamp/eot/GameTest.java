package com.bootcamp.eot;

import com.bootcamp.eot.player.ConsolePlayer;
import com.bootcamp.eot.player.Player;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameTest {
    private Game game;
    private Player player1;
    private Player player2;
    private GameConsole gameConsole;

    @Before
    public void setup() {

        gameConsole = Mockito.mock(GameConsole.class);
    }

    @Test
    public void should_test_both_console_player() {
        player1 = Mockito.mock(ConsolePlayer.class);
        player2 = Mockito.mock(ConsolePlayer.class);
        game = new Game(player1, player2, gameConsole, 1);
        when(player1.move()).thenReturn(Move.CHEAT);
        when(player2.move()).thenReturn(Move.COOPERATE);
        when(player1.getScore()).thenReturn(3);
        when(player2.getScore()).thenReturn(-1);
        game.play();
        verify(gameConsole).print("Player 1: 3, Player 2: -1");
    }

    @Test
    public void should_test_both_cheater_player() {
        player1 = Mockito.mock(ConsolePlayer.class);
        player2 = Mockito.mock(ConsolePlayer.class);
        game = new Game(player1, player2, gameConsole, 1);
        when(player1.move()).thenReturn(Move.CHEAT);
        when(player2.move()).thenReturn(Move.CHEAT);
        when(player1.getScore()).thenReturn(0);
        when(player2.getScore()).thenReturn(0);
        game.play();
        verify(gameConsole).print("Player 1: 0, Player 2: 0");
    }
}
