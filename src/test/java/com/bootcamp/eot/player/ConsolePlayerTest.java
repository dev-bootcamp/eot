package com.bootcamp.eot.player;

import com.bootcamp.eot.GameConsole;
import com.bootcamp.eot.Move;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;


public class ConsolePlayerTest {

    private Player consolePlayer;
    private GameConsole gameConsole;

    @Before
    public void setup() {
        gameConsole = Mockito.mock(GameConsole.class);
        consolePlayer = new ConsolePlayer(gameConsole);
    }

    @Test
    public void should_return_player_score() {
        assertEquals(0, consolePlayer.getScore());
    }

    @Test
    public void should_increment_player_score_by_2() {
        consolePlayer.updateScore(2);
        assertEquals(2, consolePlayer.getScore());
    }

    @Test
    public void should_perform_next_move() {
        when(gameConsole.read()).thenReturn("ch");
        Move playerMove = consolePlayer.move();
        assertEquals(Move.CHEAT, playerMove);

        when(gameConsole.read()).thenReturn("co");
        playerMove = consolePlayer.move();
        assertEquals(Move.COOPERATE, playerMove);
    }


}
