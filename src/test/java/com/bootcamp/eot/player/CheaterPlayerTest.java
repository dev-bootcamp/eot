package com.bootcamp.eot.player;

import com.bootcamp.eot.GameConsole;
import com.bootcamp.eot.Move;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

public class CheaterPlayerTest {
    private Player cheaterPlayer;

    @Before
    public void setup() {
        cheaterPlayer = new CheaterPlayer();
    }

    @Test
    public void should_return_player_score() {
        assertEquals(0, cheaterPlayer.getScore());
    }

    @Test
    public void should_increment_player_score_by_2() {
        cheaterPlayer.updateScore(2);
        assertEquals(2, cheaterPlayer.getScore());
    }

    @Test
    public void should_perform_next_move() {
        Move playerMove = cheaterPlayer.move();
        assertEquals(Move.CHEAT, playerMove);
    }
}