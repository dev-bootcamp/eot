package com.bootcamp.eot.player;

import com.bootcamp.eot.Move;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CooperatePlayerTest {
    private Player cooperatePlayer;

    @Before
    public void setup() {
        cooperatePlayer = new CooperatePlayer();
    }

    @Test
    public void should_return_player_score() {
        assertEquals(0, cooperatePlayer.getScore());
    }

    @Test
    public void should_increment_player_score_by_2() {
        cooperatePlayer.updateScore(2);
        assertEquals(2, cooperatePlayer.getScore());
    }

    @Test
    public void should_perform_next_move() {
        Move playerMove = cooperatePlayer.move();
        assertEquals(Move.COOPERATE, playerMove);
    }
}