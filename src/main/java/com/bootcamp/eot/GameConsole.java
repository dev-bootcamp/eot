package com.bootcamp.eot;

import java.io.PrintStream;
import java.util.Scanner;

public class GameConsole {

    private Scanner scanner;
    private PrintStream printStream;

    public GameConsole(Scanner scanner, PrintStream printStream) {
        this.scanner = scanner;
        this.printStream = printStream;
    }

    public String read() {
        return scanner.nextLine();
    }

    public void print(String string) {
        printStream.println(string);
    }
}
