package com.bootcamp.eot;

public class MachineOutcome {
    private int firstOutcome;
    private int secondOutcome;

    public MachineOutcome(int firstOutcome, int secondOutcome) {
        this.firstOutcome = firstOutcome;
        this.secondOutcome = secondOutcome;
    }

    public int getFirstOutcome() {
        return firstOutcome;
    }

    public int getSecondOutcome() {
        return secondOutcome;
    }
}
