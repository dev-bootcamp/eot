package com.bootcamp.eot.player;

import com.bootcamp.eot.Move;

public class CheaterPlayer extends Player {

    @Override
    public Move move() {
        return Move.CHEAT;
    }
}
