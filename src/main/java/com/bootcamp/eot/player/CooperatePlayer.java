package com.bootcamp.eot.player;

import com.bootcamp.eot.Move;

public class CooperatePlayer extends Player{

    @Override
    public Move move() {
        return Move.COOPERATE;
    }
}
