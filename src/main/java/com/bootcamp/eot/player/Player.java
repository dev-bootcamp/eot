package com.bootcamp.eot.player;


import com.bootcamp.eot.GameConsole;
import com.bootcamp.eot.Move;

public abstract class Player {

    protected int score;

    public int getScore() {
        return this.score;
    }

    public void updateScore(int points) {
        this.score += points;
    }


    public abstract Move move();
}
