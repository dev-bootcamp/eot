package com.bootcamp.eot.player;

import com.bootcamp.eot.GameConsole;
import com.bootcamp.eot.Move;

public class ConsolePlayer extends Player{
    private GameConsole gameConsole;

    public ConsolePlayer(GameConsole gameConsole) {
        super();
        this.gameConsole = gameConsole;
        this.score = 0;
    }

    public Move move() {
        String moveString = gameConsole.read();
        if(moveString.equalsIgnoreCase("ch"))
            return Move.CHEAT;
        else if(moveString.equalsIgnoreCase("co"))
            return Move.COOPERATE;
        else
            return Move.NONE;

    }
}
