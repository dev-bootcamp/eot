package com.bootcamp.eot;

import com.bootcamp.eot.player.CheaterPlayer;
import com.bootcamp.eot.player.ConsolePlayer;
import com.bootcamp.eot.player.CooperatePlayer;
import com.bootcamp.eot.player.Player;

import java.io.PrintStream;
import java.util.Scanner;

public class Game {

    private int rounds;
    private GameConsole gameConsole;
    private Player player1;
    private Player player2;
    private Machine machine;


    public Game(Player player1, Player player2, GameConsole gameConsole, int rounds) {
        this.machine = new Machine();
        this.player1 = player1;
        this.player2 = player2;
        this.rounds = rounds;
        this.gameConsole = gameConsole;
    }

    public void play() {
        for (int i = 0; i < rounds; i++) {
            if (player1.move().equals(Move.NONE)) {
                System.out.printf("Invalid Input Exiting .....");
                break;
            } else {
                MachineOutcome machineOutcome = machine.process(player1.move(), player2.move());
                player1.updateScore(machineOutcome.getFirstOutcome());
                player2.updateScore(machineOutcome.getSecondOutcome());

                gameConsole.print("Player 1: " + player1.getScore() + ", Player 2: " + player2.getScore());
            }
        }
    }


    public static void main(String[] args) {
        GameConsole gameConsole = new GameConsole(new Scanner(System.in), new PrintStream(System.out));
        Game game = new Game(new CheaterPlayer(), new CooperatePlayer(), gameConsole, 5);
        game.play();
    }
}